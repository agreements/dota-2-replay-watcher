#!/usr/bin/python3.2
__author__ = 'shyo'

import json
import webbrowser
from urllib.request import urlopen
import time
import tkinter as tk

with open('apikey.txt', 'r') as file:

    APIKEY = file.readline()
file.close()




class ReplayWatcher(tk.Tk):
    def __init__(self, master=None):
        tk.Tk.__init__(self, master)
        self.grid()
        self.initialize()
        self.LeagueDict = self.UpdateLeagues()
        self.OrderLeagueDict(self.LeagueDict)

    def initialize(self):

        self.MatchIDVar = tk.StringVar()
        self.TeamNameVar = tk.StringVar()

        self.MEntry = tk.Entry(self, textvariable=self.MatchIDVar)
        self.MEntry.grid(column=0, row=0, columnspan=6, sticky='EW')
        self.MEntry.bind("<Return>", self.StartReplay)
        self.MatchIDVar.set(u'Enter Match ID here')
        #self.MatchID = self.MatchIDVar.get() #not needed atm

        #self.TNEntry = tk.Entry(self, textvariable=self.TeamNameVar)
        #self.TNEntry.grid(column = 0, row = 1, columnspan = 6, sticky='EW')
        #self.TNEntry.bind("<Return>", self.GetLeagueInfo)
        #self.TeamNameVar.set(u'Enter Team Name here')
        #self.TeamName = self.TeamNameVar.get()


       # self.wait_timer = 300 #time in seconds to wait until next replay plays
        

        GoButton = tk.Button(self, text=u'Watch Game', command=self.StartReplay)
        GoButton.grid(column = 5, row = 0, sticky='EW')

        #WatchAll = tk.Button(self, text=u'Watch All', command=self.WatchTeamReplays)
        #WatchAll.grid(column = 5, row = 1, sticky='EW')


        GetInfo = tk.Button(self, text=u'Match Info', command=self.GetInfo)
        GetInfo.grid(column = 3, row = 2, columnspan=3, sticky = 'EW')



        Instruction = tk.Label(self, text='Choose League')
        Instruction.grid(column = 0, row = 2, sticky = 'EW')



        self.leaguelist = tk.Listbox(self, selectmode = 'SINGLE')
        self.leaguelist.grid(column = 0, row = 3, columnspan = 4, rowspan = 1, sticky='NSEW')

        self.matchlist = tk.Listbox(self, selectmode = 'SINGLE')
        self.matchlist.grid(column = 0, row = 4, columnspan = 4, sticky='NSEW')

        self.infolb = tk.Listbox(self, selectmode = 'MULTIPLE')
        self.infolb.bind("<<ListboxSelect>>")
        self.infolb.grid(column = 3, row = 3, columnspan = 4, rowspan = 6, sticky='NSEW')



        self.leaguelist.bind('<<ListboxSelect>>', self.onSelectLeague)
        self.matchlist.bind('<<ListboxSelect>>', self.onSelectMatch)



        self.scrollbar_L = tk.Scrollbar(self)
        self.scrollbar_R = tk.Scrollbar(self)
        self.scrollbar_D = tk.Scrollbar(self)


        self.scrollbar_L.config(command=self.leaguelist.yview)
        self.scrollbar_R.config(command=self.infolb.yview)
        self.scrollbar_D.config(command=self.matchlist.yview)

        self.leaguelist.config(yscrollcommand=self.scrollbar_L.set)
        self.infolb.config(yscrollcommand=self.scrollbar_R.set)
        self.matchlist.config(yscrollcommand=self.scrollbar_D.set)


        self.scrollbar_L.grid(column=1,row = 3, sticky='ENS')
        self.scrollbar_R.grid(column=5,row = 3, rowspan = 2, sticky='ENS')
        self.scrollbar_D.grid(column=1,row = 4, sticky='ENS')



        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(4, minsize=150, weight=1)
        self.grid_rowconfigure(3, minsize=300, weight=1)
        self.grid_columnconfigure(1, minsize=250)

        self.resizable(True, True)
        self.update()
        self.geometry(self.geometry())


        self.update()
        self.geometry(self.geometry())





        self.MEntry.focus_set()
        self.MEntry.selection_range(0, tk.END)

    def onSelectLeague(self, evt):
        w = evt.widget
        index = int(w.curselection()[0])
        value = w.get(index)
        from_id = value.find('ID')

        league_name = value[:from_id]
        league_id = value[from_id:]
        league_id  = league_id[3:] #if you tell me a better solution for this i will send you 5 dollars
        self.GetLeagueInfo(league_id, league_name)

    def onSelectMatch(self, evt):
        w = evt.widget
        index = int(w.curselection()[0])
        value = w.get(index)
        self.MatchIDVar.set(value)
        self.GetInfo()







    def StartReplay(self):
        ie = webbrowser.get(webbrowser.iexplore)
        ie.open('dota2://matchid='+self.MatchIDVar.get())
        time.sleep(self.wait_timer)

    def GetInfo(self):

        """Grab match info from Valves Dota 2 API and display all relevant information in a neat display"""
        url = 'https://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/V001/?key='+APIKEY+'&match_id='+self.MatchIDVar.get()
        page = urlopen(url).read()
        decoded_data = json.loads(page.decode())
        self.infolb.delete(0, tk.END) #clears the LB first

        for i in decoded_data['result'].items(): #do a proper display here
            k = str(i)

            split = k.find(',')
            a = k[:split]
            b = k[split:]

            self.infolb.insert(tk.END, i)


    def GetLeagueInfo(self, league_id, league_name):
        """Returns all Matches in a given league"""
        start_id = '0'
        current_matches = [league_id]

        def get_one_page(start_id): #hacky


            url = 'https://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/V001/?key='+APIKEY+'&league_id='+str(league_id)+'&start_at_match_id='+start_id
            page = urlopen(url).read()
            decoded_data = json.loads(page.decode())
            total_results = decoded_data['result']['total_results']

            results_remaining = decoded_data['result']['results_remaining']
            match_info = decoded_data['result']['matches']
            print('Currently parsing league ...',league_id, league_name, '# of Matches:',total_results,'remaining Matches:',results_remaining) #first item in list is league id
            for contents in match_info:
                current_matches.append(contents['match_id'])
            if results_remaining > 0:
                start_id = str(current_matches[-1]) #start the match search from the last match on the previous page
                get_one_page(start_id)
        get_one_page(start_id)
        self.matchlist.delete(0, tk.END)

        #reformat everything before it gets inserted (LeagueName\n, MatchID, Team 1 vs Team 2\n, ...)
        self.matchlist.insert(tk.END, 'League Name:'+league_name)

        print(current_matches)
        i = 2
        while i <= len(current_matches):
            i =+ i+1
            self.matchlist.insert(tk.END, current_matches[i - 2])


    def TeamInList(self):

        ChosenMatch = self.GetInfo()
        if self.TeamName in ChosenMatch:
            pass

    def WatchTeamReplays(self):
        pass

    def OrderLeagueDict(self, dict):
        ordered_leagues = []

        for i, id in dict.items():

            id = str(id)
            k = str(i)
            k = k[11:]

            for char in k:
                if char == '_':
                    k = k.replace(char, ' ')
                if char in "'}){#":
                    k = k.replace(char, '')

            ordered_leagues.append(' '+k+' ID:'+id)

        ordered_leagues.sort()
        for i in ordered_leagues:
            self.leaguelist.insert(tk.END, i)


    def UpdateLeagues(self):
        """Calls the Steam API for all Dota 2 leagues and creates a list"""
        self.infolb.delete(0, tk.END)  #clears the LB first
        url = 'https://api.steampowered.com/IDOTA2Match_570/GetLeagueListing/v0001/?key='+APIKEY
        page = urlopen(url).read()
        decoded_data = json.loads(page.decode())
        league_info = decoded_data['result']['leagues']
        LeagueDict = {}
        for a in league_info:
            key = a['name']
            value = a['leagueid']
            LeagueDict[key] = value

        return LeagueDict

if __name__ == '__main__':
    app = ReplayWatcher(None)
    app.title('Dota 2 Replay Watch')
    app.mainloop()
